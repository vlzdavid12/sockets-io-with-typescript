
import express from 'express';
import { SERVER_PORT } from '../global/environment';
import socketIO from 'socket.io';
import http from 'http';

import * as socket from '../sockets/socket';

export default class Server {

    private static _instance: Server;

    public app: express.Application;
    public port: number;

    public io: socketIO.Server;
    private httpServer: http.Server;


    private constructor() {
        this.app = express();
        this.port = SERVER_PORT;
        this.httpServer = new http.Server( this.app );
        this.io = socketIO( this.httpServer );

        this.callSockets();
    }

    public static get instance() {
        return this._instance || ( this._instance = new this() );
    }


    private callSockets() {

        console.log('Call connect - sockets');

        this.io.on('connection', client => {

            //Connect Client
            socket.connectClient( client, this.io);

            //console.log('Client Connect!');
            // Config
            socket.configUser( client, this.io );

            // Message
            socket.message( client, this.io );

            // Message
            socket.getUser( client, this.io );


            // Disconnect
            socket.disconnect( client, this.io );

        });

    }


    start( callback: Function ) {

        this.httpServer.listen( this.port, ()=>callback );

    }

}
