export class User{
  public id: string;
  public  name: string;
  public livingRoom: string;

  constructor(id: string) {
    this.id = id;
    this.name = 'not-name';
    this.livingRoom = 'not-liveRoom';
  }
}
