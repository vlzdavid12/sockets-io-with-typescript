import {User} from './user';

export class UserList {
  private list: User[] = [];

  constructor() {
  }

  // Add new user
  public addUser(user: User): User {
    this.list.push(user);
    console.log(this.list);
    return user;
  }

  // Update Name
  public updateName(id: string, name: string): void {

    for (let user of this.list) {
      if (user.id === id) {
        user.name = name;
        break;
      }
    }


    console.log('==== Update User ====');
    console.log(this.list);
  }

  // Get List of User
  public getList(): User[]{
    return this.list.filter(user => user.name !== 'not-name');
  }

  // Get User
  public getUser(id: string): User | undefined{
    return this.list.find(user => user.id === id);
  }

  // Get All User in Living Room
  public getUsersLivingRoom(room: string): User[] {
    return this.list.filter( (user) => user.livingRoom === room);
  }

  // GeT Erase User
  public eraseUser(id: string): User | undefined{
    const userTmp =  this.getUser( id );
    this.list =  this.list.filter(user => user.id !== id);
    return userTmp;
  }
}
