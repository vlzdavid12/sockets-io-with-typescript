
import { Router, Request, Response } from 'express';
import Server from "../classes/server";
import {userConnects} from "../sockets/socket";

const router = Router();

router.get('/messages', ( req: Request, res: Response  ) => {

    res.json({
        ok: true,
        message: 'Very Good!!'
    });

});

router.post('/messages', ( req: Request, res: Response  ) => {

    const body = req.body.body;
    const of     = req.body.of;

    const server = Server.instance;

    const payload = {
        body: body,
        of: of
    }

    server.io.emit('new-message', payload)

    res.json({
        ok: true,
        body,
        of
    });

});


router.post('/messages/:id', ( req: Request, res: Response  ) => {

    const body = req.body.body;
    const of    = req.body.of;
    const id     = req.params.id;

    const server = Server.instance;

    const payload =  { of, body}

    server.io.in(id).emit('message-private', payload)

    res.json({
        ok: true,
        body,
        of,
        id
    });

});

// Service Users for id
router.get('/users/', ( req: Request, res: Response  ) => {

    const server = Server.instance;

    server.io.clients((err: any, clients: string[])=>{
        if(err){
            return res.json({
                ok: false,
                msg: err
            });
        }

        res.json({
            ok: true,
            clients
        });
    })

});

router.get('/users/detail', ( req: Request, res: Response  ) => {

        res.json({
            ok: true,
            clients:  userConnects.getList()
        });


});



export default router;


