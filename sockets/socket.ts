import {Socket} from 'socket.io';
import socketIO from 'socket.io';
import {UserList} from "../classes/userList";
import {User} from "../classes/user";

export const userConnects = new UserList();

export const disconnect = (client: Socket, io: socketIO.Server) => {

    client.on('disconnect', () => {
        console.log('Client Disconnect!');
        userConnects.eraseUser(client.id);
        // Send emit list clients connect
        io.emit('actives-users', userConnects.getList());
    });

}

export const connectClient = (client: Socket, io: socketIO.Server) => {
    const user = new User(client.id);
    userConnects.addUser(user);


}


// Call Messages
export const message = (client: Socket, io: socketIO.Server) => {

    client.on('message', (payload: { of: string, body: string }) => {

        console.log('Received Message', payload);
        // Send Messages of Frond
        io.emit('new-message', payload);

    });
}


// Config User
export const configUser = (client: Socket, io: socketIO.Server) => {

    client.on('config_user', (payload: { name: string }, callback: Function): void => {

        userConnects.updateName(client.id, payload.name);

        // Send emit list clients connect
        io.emit('actives-users', userConnects.getList());

        callback({
            ok: true,
            msg: `User ${payload.name}, config!`
        })
    });

}

// Get Users
export const getUser = (client: Socket, io: socketIO.Server) => {

    client.on('get_users', (): void => {
        io.in(client.id).emit('actives-users', userConnects.getList());
    });

}


